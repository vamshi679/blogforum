import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  // errorMsg='';
  userdetails: any;
  // options:any = [
  //   {CommunityName:"option a"},{ CommunityName:"option b"},{CommunityName:"option c"}
  // ]   
  options:any;
  
  constructor(private _blogservice: BlogService, private _router: Router) { }
  
  ngOnInit() {
    this._blogservice.getcommunity().subscribe(res1 => {
      this.options = res1
      console.log('success', res1)
    })
  }

  onSubmit(form) {

    this._blogservice.userRegister(form).subscribe(data => {
      console.log('registration sucess');
      this._router.navigate(['/login'])
    },
      err => {
        console.log("registration-failed");
      }
    );
  }

}



// ,error => this.errorMsg= error.status