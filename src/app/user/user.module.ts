import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { FormsModule } from '@angular/forms';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';


@NgModule({
  declarations: [UserdashboardComponent],
  imports: [
    CommonModule,
    UserRoutingModule,FormsModule
  ]
})
export class UserModule { }
