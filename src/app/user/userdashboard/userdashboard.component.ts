import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/blog.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-userdashboard',
  templateUrl: './userdashboard.component.html',
  styleUrls: ['./userdashboard.component.css']
})
export class UserdashboardComponent implements OnInit {

  usrId:any;
  post:any;
  userobj:any;


  constructor(private _blogservice:BlogService,private activeroute:ActivatedRoute) { }

  ngOnInit() {
    var userdetails=localStorage.getItem('usrdet')
    var tojson = JSON.parse(userdetails);
    this.userobj=tojson
      this.activeroute.params.subscribe(params => {
      this.usrId= params['id'];
    });

    this._blogservice.getuserblogById(this.usrId).subscribe(data => {
      this.post=data
      console.log('success')
    },(err: any) => {
      console.log('Failure Response');
    })
  }

}

// var userobj = JSON.parse(json);