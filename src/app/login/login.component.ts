import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  faketoken: string;
  userid: string;

  constructor(private _blogservice: BlogService, private _router: Router) {
    this.loginForm = new FormGroup({
      username: new FormControl(''),
      password: new FormControl(''),
      userType: new FormControl('')
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this._blogservice.logout();
    }, 0);
  }


  onSubmit() {
    this._blogservice.userlogin(this.loginForm.value).subscribe(data => {
      console.log(typeof (data))
      var json = JSON.parse(data);

      if (this.loginForm.value.userType == 'admin') {
        alert('login success');
        this.faketoken = "xx.yy.zzadmin"
        localStorage.setItem('token', this.faketoken);
        this.userid = json.userId
        this._router.navigate(['/admindashboard'])

      }
      else if (this.loginForm.value.userType == 'user') {
        alert('login success');
        this.faketoken = "xx.yy.zzuser"
        localStorage.setItem('token', this.faketoken);
        this.userid = json.userId
        // localStorage.setItem('usrdet',json)
        localStorage.setItem('usrdet',JSON.stringify(json))
        this._router.navigate([`/userdashboard/${this.userid}`])
      }
      else {
        console.log('Login failed');
        alert('Login failed please select usertype');
      }
    });
  }

}