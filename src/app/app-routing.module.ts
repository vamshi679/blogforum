import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { MyCommentComponent } from './my-comment/my-comment.component';
import { MyTopicComponent } from './my-topic/my-topic.component';
import { PostComponent } from './post/post.component';
import { AuthGuard } from './auth.guard';


const routes: Routes = [{path:'',redirectTo:'home',pathMatch:'full'},{path:'home',component:HomeComponent},
                      {path:'login',component:LoginComponent},{path:'register',component:UserRegisterComponent},
                      {path:'mycomment',component:MyCommentComponent},{path:'mytopic',component:MyTopicComponent},
                      {path:'postsbyid/:id',component:PostComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
