import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import {catchError} from 'rxjs/operators'
import {throwError} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  private loginUrl="http://localhost:8080/login";
  private registerURL="http://localhost:8080/adduser";

  private addpostURL="http://localhost:8080/addBlog";
  private allpostsURL="http://localhost:8080/getAllBlogs";
  private acceptedpostsURL="http://localhost:8080/getAllApprovedBlogs";

  private addcommunityURL="http://localhost:8080/addCommunity";
  private getcommunityURL="http://localhost:8080/getAllCommunities";
  
  private commentURL="http://localhost:8080/createComment";
  
  private allcommentsURL=""
  
  private addtopicURL="http://localhost:8080/addTopic";


  constructor(private _hc: HttpClient,private _router:Router) { }

  userlogin(x):Observable<any>{
    console.log(x)
    return this._hc.post(this.loginUrl,x ,{responseType: 'text'})
  }

  userRegister(y):Observable<any>{
    return this._hc.post(this.registerURL,y)
  }

  // errorHandler(error:HttpErrorResponse){
  //   return throwError(error);
  // }

  loggedIn():boolean{
    return !!localStorage.getItem('token')
  }

  logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('usrdet')
    this._router.navigate(['/login'])
  }

  addPost(post):Observable<any>{
    return this._hc.post(this.addpostURL,post)
  }

  getallposts():Observable<any>{
    return this._hc.get(this.allpostsURL)
  }

  approvedBlogs():Observable<any>{
    return this._hc.get(this.acceptedpostsURL)
  }

  getuserblogById(blogid):Observable<any>{
    return this._hc.get(`http://localhost:8080/getUserBlog/${blogid}`)
  }

  addcomment(Comment):Observable<any>{
    return this._hc.post(this.commentURL,Comment)
  }

  approveBolgnyID(blgidA,post):Observable<any>{
    return this._hc.post(`http://localhost:8080/getBlogById/${blgidA}`,post)
  }

  rejectBolgbyID(blgidR,post):Observable<any>{
    return this._hc.post(`http://localhost:8080/getBlogById/${blgidR}`,post)
  }

  deleteBlogbyId(delblg):Observable<any>{
    return this._hc.delete(`http://localhost:8080/getBlogById/${delblg}`)
  }

  // add new community
  addcommunity(comname):Observable<any>{
    return this._hc.post(this.addcommunityURL,comname)
  }

  // get all communities
  getcommunity():Observable<any>{
    return this._hc.get(this.getcommunityURL)
  }

  // get all comments
  getallcomments():Observable<any>{
    return this._hc.get(this.allcommentsURL)
  }

  // get comment by userid
  getcommentsbyid(id):Observable<any>{
    return this._hc.get(`http://localhost:8080/getallblogComments/${id}`)
  }

  deletecommentbyId(id):Observable<any>{
    return this._hc.delete(`http://localhost:8080/deleteComment/${id}`)
  }

  addTopic(topicname):Observable<any>{
    return this._hc.post(this.addtopicURL,topicname)
  }

  getcommunitybyID(id):Observable<any>{
    return this._hc.get(`http://localhost:8080/getCommunityById/${id}`)
  }


}
