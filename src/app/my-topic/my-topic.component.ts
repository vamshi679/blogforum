import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-my-topic',
  templateUrl: './my-topic.component.html',
  styleUrls: ['./my-topic.component.css']
})
export class MyTopicComponent implements OnInit {

  norow: boolean = true
  article: boolean = false;
  userobj:any;
  commlist:any;

  constructor(private _blogService: BlogService, private _router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    var userdetails=localStorage.getItem('usrdet')
    var tojson = JSON.parse(userdetails);
    this.userobj=tojson

    this._blogService.getcommentsbyid(this.userobj.userId).subscribe(res1=>{
      console.log('success')
      this.commlist=res1
    },err=>{
      console.log('failed',err)
    }
    )
  }

  addPost(blog) {
    console.log(blog)
    this._blogService.addPost(blog).subscribe(data => {
      if (data) {
        alert("blog posted success")
      }
    }, error => {
      console.log('Failure Response', error);
    })
  }

  addtopic(tname) {
    console.log(tname)
    this._blogService.addTopic(tname).subscribe(
      resp => {
        alert('topic added')
      }, err => {
        console.log('failed', err)
      }
    )
  }

  changetoarticle() {
    this.article = true;
    this.norow = false;
  }

  changetoblog() {
    this.norow = true;
    this.article = false;
  }


}
