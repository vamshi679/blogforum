import { Injectable } from '@angular/core';
import { CanActivate, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { BlogService } from './blog.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private _blogservice:BlogService,private _router:Router){}

  canActivate():boolean{
    if(this._blogservice.loggedIn()){
      return true
    }
    else{
      this._router.navigate(['/login'])
      return false
    }
  }
  
}
