import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogService } from '../blog.service';

import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  paramLink: string;
  post:any;
  userobj;any;
  usercomments:any;

  constructor(private activeroute:ActivatedRoute,private _blogservice:BlogService,private _router:Router) {
   }

  ngOnInit() {
    var userdetails=localStorage.getItem('usrdet')
    var tojson = JSON.parse(userdetails);
    this.userobj=tojson

    // getting request based on blogid
    this.activeroute.params.subscribe(params => {
      this.paramLink = params['id'];
      // console.log(this.paramLink)
    });

    this._blogservice.approvedBlogs().subscribe(data => {
      for(let p of data){
        if(p.blogId==this.paramLink)
        {
          this.post=p
        } 
      }
    },(err: any) => {
      console.log('Failure Response');
    })

    this._blogservice.getcommentsbyid(this.userobj.userId).subscribe(
      resl=>{
        console.log('success')
        this.usercomments=resl
      },
      err=>{
        console.error(err,'failed')
      }
    )
   
  }

  OnClick(comment){
    comment.userId=this.userobj.userId
    comment.blogId=this.post.blogId
    if(this._blogservice.loggedIn()==true){
      this._blogservice.addcomment(comment).subscribe(data1=>{
        console.log('success',data1)
      })
    }
    else{
      alert('oops it seems you have not logged in,please signin or signup to comment on this post')
      this._router.navigate(['/login'])
    }
  }



}
