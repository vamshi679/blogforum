import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-comment',
  templateUrl: './my-comment.component.html',
  styleUrls: ['./my-comment.component.css']
})
export class MyCommentComponent implements OnInit {

  usrId: any;
  userdetils: any;
  userobj:any;
  comments: any;

  constructor(private _blogservice: BlogService, private activeroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeroute.params.subscribe(params => {
      this.usrId = params['id'];
    });

    var userdetails=localStorage.getItem('usrdet')
    var tojson = JSON.parse(userdetails);
    this.userobj=tojson

    this._blogservice.getcommentsbyid(this.usrId).subscribe(
      res => {
        this.comments = res;
        console.log('success')
      }, err => { console.error('failed', err) });
  }

  deletecomment(cmt){
    cmt.userId=this.userobj.userId
    this._blogservice.deletecommentbyId(cmt).subscribe(
      resp=>{
        alert('success')
      },
      err=>{
        console.log('failed',err)
      }
    )
  }



}
