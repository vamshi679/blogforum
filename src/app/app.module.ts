import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { LoginComponent } from './login/login.component';
import { MyCommentComponent } from './my-comment/my-comment.component';
import { MyTopicComponent } from './my-topic/my-topic.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { BlogService } from './blog.service'

import { EditorModule } from '@tinymce/tinymce-angular';
import { AdminModule } from './admin/admin.module';
import { UserModule } from './user/user.module';
import { AuthGuard } from './auth.guard';
import { PostComponent } from './post/post.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserRegisterComponent,
    LoginComponent,
    MyCommentComponent,
    MyTopicComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, FormsModule, HttpClientModule, EditorModule, AdminModule, UserModule, ReactiveFormsModule, BrowserAnimationsModule,
    MatFormFieldModule,MatInputModule,MatSelectModule,MatButtonModule
  ],
  providers: [BlogService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
