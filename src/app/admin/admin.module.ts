import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms'

import { AdminRoutingModule } from './admin-routing.module';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [AdmindashboardComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,FormsModule,MatSelectModule
  ]
})
export class AdminModule { }
