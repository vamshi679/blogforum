import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/blog.service';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent implements OnInit {

  constructor(private _blogservice: BlogService) { }

  postselection: any;
  commoptions:any;
  allcomments:any;
  // Array1: any = [];
  allblogs:any=[];
  topicArray:any;  
  deletedpost: any;

  blogs:boolean=true;
  topics:boolean=false;
  comments:boolean=false;


  ngOnInit() {
    this._blogservice.getallposts().subscribe(res => {
      this.postselection = res
      console.log('success', res)
    })

    this._blogservice.getcommunity().subscribe(res1 => {
      this.commoptions = res1
      this.topicArray=this.commoptions.topics;
      console.log('success', res1)
    })

    this._blogservice.getallcomments().subscribe(res2 => {
      this.allcomments = res2
      console.log('success', res2)
    })

  }

  changetoTopics(){
    this.topics=true
    this.blogs=false;
  }

  changetoComments(){
    this.comments=true;
    this.topics=false;
    this.blogs=false
  }

  topicstoBlogs(){
    this.blogs=true;
    this.topics=false;
  }

  commentstoBlogs(){
    this.blogs=true;
    this.comments=false
  }

  getmembers(c){
    console.log(c)
    for(let list of this.postselection){
      if(list.communityName==c){
        this.allblogs.push(list)
      }
    }
  }

  approveBlog(apblg,post) {
    console.log(apblg)
    this._blogservice.approveBolgnyID(apblg,post).subscribe(data => {
      if (data) {
        alert('approved sucess');
        this.ngOnInit();
      }
    },
      err => {
        alert("request failed");
      }
    );
  }

  rejectBlog(rjblg,post) {
    console.log(rjblg)
    this._blogservice.rejectBolgbyID(rjblg,post).subscribe(data => {
      if (data) {
        alert('rejected sucess');
        this.ngOnInit();
      }
    },
      err => {
        alert("request failed");
      }
    );
  }

  deleteBlog(delblg){
    console.log(delblg)
    this._blogservice.deleteBlogbyId(delblg).subscribe(data => {
      if (data) {
        alert('post deleted');
        this.ngOnInit();
      }
    },
      err => {
        alert("request failed");
      }
    );
  }

  addCommunity(addname){
    // console.log(addname.value)
    // this.commArray.push(addname.value)
    this._blogservice.addcommunity(addname).subscribe(data => {
      if (data) {
        console.log('success');
        this.ngOnInit();
      }
    },
      err => {
        console.log("request failed");
      }
    );
  }
  

}
