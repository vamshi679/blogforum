import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  approvedposts:any;

  constructor(private _blogservice:BlogService) { }

  ngOnInit() {
    this._blogservice.approvedBlogs().subscribe(res=>{
      this.approvedposts=res
      console.log(this.approvedposts)
    })
  }

}
